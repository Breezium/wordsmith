# Ms Wordsmith (Gameplay Test)

## Goals
The main goal of this app is to create a scroller view with a collection of stickers where the user can pick one and, through drag & drop gesture, deploy it into another canvas.

The structure must be arranged in the following way:

* Stickers.unity is the scene containing the scrolling view with the stickers.

* Canvas.unity is the scene containing a canvas where the stickers will be deployed.

* The stickers in the scroll view must be stored in a List<Sticker> AvailableStickers.
­ 
* The stickers in the Canvas must be stored in a List<Sticker> StickersInUse.

* The user must be able to drag a sticker from one scene to the other.

* The user must be able to scroll through the list of stickers vertically.

* The sticker must “detach” from AvailableStickers after a long press, then start the drag
gesture.

* Dragging a sticker out of the scrolling view should not leaving “holes” in the sticker list.

* A replacement is needed as soon as the drag is starting.

* The user must be able to drag a sticker out with a smooth movement that doesn’t break
scrolling view experience.

* Given a dragged sticker, on user release in the Canvas area, the sticker must “attach” to
StickersInUse and become children of the Canvas.

* Using Singletons is discouraged and extra points will be given to alternative solutions
that don’t make use of them.

## Results

* The Main scene is called SampleScene in Scenes directory.

* I think I've met the criteria set.

* Additively load 2 separate scenes, Stickers.unity & Canvas.Unity

* Stickers are stored in List<Sticker> AvailableStickers & List<Sticker> StickersInUse.

* Can scroll through the sticker palette, new colours for stickers can be made by adding the colour to stickerColourList in the draggableStickerGenerator in the Sticker scene.

* Stickers created in the stickers Scene are added to the Canvas scene hierarchy.

* Press and hold for a second before Sticker is detached from Sticker palette.

* No Singletons