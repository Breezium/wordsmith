﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DraggableStickerGenerator : MonoBehaviour
{
    public List<GameObject> AvailableStickers = new List<GameObject>();

    public GameObject stickerPrefab;
    public List<Color> stickerColorList;

    public bool bMakeClickableStickers;

    public Vector2 iconOffset;

    MessageGenerator messageGenerator;

    // Start is called before the first frame update
    void Awake()
    {
        messageGenerator = FindObjectOfType<MessageGenerator>();
    }

    public void Start()
    {
        Vector2 pos = Vector2.zero;
        for (int i = 0; i < stickerColorList.Count; i++)
        {
            GameObject sticker = SpawnSticker(pos, i);
            pos -= iconOffset;

            DraggableSticker draggableSticker = sticker.GetComponent<DraggableSticker>();
            draggableSticker.messageGenerator = messageGenerator;

            AvailableStickers.Add(sticker);
        }
    }

    public GameObject SpawnSticker(Vector2 pos, int i)
    {
        return SpawnSticker(pos, stickerColorList[i]);
    }

    public GameObject SpawnSticker(Vector2 pos, Color col)
    {
        GameObject stickerGO = Instantiate<GameObject>(stickerPrefab, transform.position + new Vector3(pos.x, pos.y, 0), Quaternion.identity, transform);

        Sticker sticker = stickerGO.GetComponent<Sticker>();
        sticker.color = col;

        return stickerGO;
    }
}

