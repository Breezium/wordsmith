﻿using UnityEngine;

public class MessageHandler : MonoBehaviour
{
    StickerGenerator stickerGenerator;
    // Start is called before the first frame update
    void Start()
    {
        stickerGenerator = GetComponent<StickerGenerator>();

    }

    internal void Log(string v)
    {
        Debug.Log(v);
    }

    public void Spawn(Color col, Vector2 position)
    {
        stickerGenerator.SpawnSticker(position, col);
    }
}
