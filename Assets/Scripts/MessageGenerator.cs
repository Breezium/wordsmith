﻿using UnityEngine;

public class MessageGenerator : MonoBehaviour
{
    MessageHandler messageHandler;

    // Start is called before the first frame update
    void Start()
    {
        messageHandler = FindObjectOfType<MessageHandler>();
    }
    public void Communicate()
    {
        messageHandler.Log("Test");
    }

    internal void SpawnSticker(Color col, Vector2 position)
    {
      //  Debug.Log("Icon Pos = " + position);
        messageHandler.Spawn(col, position);
    }
}
