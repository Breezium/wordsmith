﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DemoManager : MonoBehaviour
{
    public string loadCanvasLevelAdditively;
    public string loadStickerLevelAdditively;
    // Start is called before the first frame update
    void Awake()
    {
        SceneManager.LoadScene(loadCanvasLevelAdditively, LoadSceneMode.Additive);
        SceneManager.LoadScene(loadStickerLevelAdditively, LoadSceneMode.Additive);
    }
}
