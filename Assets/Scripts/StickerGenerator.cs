﻿using System.Collections.Generic;
using UnityEngine;

public class StickerGenerator : MonoBehaviour
{
    public List<Sticker> StickersInUse = new List<Sticker>();
    public GameObject stickerPrefab;

    MessageGenerator messageGenerator;

    // Start is called before the first frame update
    void Awake()
    {
        messageGenerator = FindObjectOfType<MessageGenerator>();
    }

    public GameObject SpawnSticker(Vector2 pos, Color col)
    {

   //     Debug.Log("Spawn Sticker Pos = " + pos);
        GameObject stickerGO = Instantiate<GameObject>(stickerPrefab,  new Vector3(pos.x, pos.y, 0), Quaternion.identity, transform);

        Sticker sticker = stickerGO.GetComponent<Sticker>();
        sticker.color = col;

        StickersInUse.Add(sticker);

        return stickerGO;
    }
}
