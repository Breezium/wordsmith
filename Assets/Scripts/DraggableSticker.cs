﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableSticker : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public bool dragOnSurfaces = true;

    private GameObject m_DraggingIcon;
    private RectTransform m_DraggingPlane;

    public MessageGenerator messageGenerator;
    public float holdDuration = 1f;
    public bool inputHold = false;
    bool invalidDrag = false;
    float holdTimer;

    PointerEventData cachedEventData;

    public void OnPointerDown(PointerEventData eventData)
    {
        inputHold = true;
        holdTimer = holdDuration;
        cachedEventData = eventData;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        inputHold = false;
        invalidDrag = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
     //   Debug.Log("OnBeginDrag");
        cachedEventData = eventData;
    }

    private void InitialiseDrag(PointerEventData eventData)
    {
        if (invalidDrag)
            return;

        var canvas = FindInParents<Canvas>(gameObject);
        if (canvas == null)
            return;

        // We have clicked something that can be dragged.
        // What we want to do is create an icon for this.
        m_DraggingIcon = new GameObject("icon");

        m_DraggingIcon.transform.SetParent(canvas.transform, false);
        m_DraggingIcon.transform.SetAsLastSibling();

        var image = m_DraggingIcon.AddComponent<Image>();

        image.sprite = GetComponent<Image>().sprite;
        image.SetNativeSize();

        image.color = GetComponent<Image>().color;

        if (dragOnSurfaces)
            m_DraggingPlane = transform as RectTransform;
        else
            m_DraggingPlane = canvas.transform as RectTransform;

        SetDraggedPosition(eventData);
    }

    public void OnDrag(PointerEventData data)
    {
    //    Debug.Log("UpdatingDrag");
        if (inputHold)
        {
            //cachedEventData = data;
            invalidDrag = true;
            return;
        }

        if(m_DraggingIcon != null)
            SetDraggedPosition(data);
    }

    private void SetDraggedPosition(PointerEventData data)
    {
        if (dragOnSurfaces && data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
            m_DraggingPlane = data.pointerEnter.transform as RectTransform;

        var rt = m_DraggingIcon.GetComponent<RectTransform>();
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane, data.position, data.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = m_DraggingPlane.rotation;
        }
    }

    void Update()
    {
        if(inputHold)
        {
            holdTimer -= Time.deltaTime;
          
            if(holdTimer <= 0)
            {
                inputHold = false;

                InitialiseDrag(cachedEventData);
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (m_DraggingIcon != null)
        {
            SetDraggedPosition(eventData);

            var rt = m_DraggingIcon.GetComponent<RectTransform>();

            messageGenerator.SpawnSticker(GetComponent<Image>().color,rt.position);
            Destroy(m_DraggingIcon);
        }
        //inputHold = false;
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        Transform t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }
}